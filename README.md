# bulk-demo

## 项目说明
本项目是一些示例的合集，每一个都是独立的项目

主要做一些中间件和 Spring Boot 的整合预计实际应用示例


更多信息文档参考 https://doc.bulkall.top/

后续会慢慢补充

## 目录简介

|项目|描述|
| ---------------- | -------------------------- |
|[spring-boot-bloom-filter](./spring-boot-bloom-filter/README.md)| Spring Boot 整合使用布隆过滤器演示|
|[spring-boot-websocket](./spring-boot-websocket/README.md)| Spring Boot 整合使用 Socket & 在线聊天演示|
|[spring-boot-lock](./spring-boot-lock)| Spring Boot 整合各种分布式锁演示|
|[spring-boot-lock-zookeeper](./spring-boot-lock/spring-boot-lock-zookeeper/README.md)| Spring Boot 整合 Zookeeper 分布式锁演示|
|[spring-boot-lock-redis](./spring-boot-lock/spring-boot-lock-redis/README.md)| Spring Boot 整合 Redis 分布式锁演示|
|[spring-boot-lock-mysql](./spring-boot-lock/spring-boot-lock-mysql/README.md)| Spring Boot 基于 MySQL 实现分布式锁（乐观锁，悲观锁）演示|
|[spring-boot-mq](./spring-boot-mq/README.md)| Spring Boot 中各种 MQ 使用演示|
|[spring-boot-mq-redis](./spring-boot-mq/spring-boot-mq-redis/README.md)| Spring Boot 基于 Redis 的 list、pub/sub、stream 三种方式实现 MQ 演示|


## 后续计划
- [] 集成MQ