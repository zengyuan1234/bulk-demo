package top.bulk.mq.redis.annotation;

import java.lang.annotation.*;

/**
 * 监听注解
 *
 * @author 散装java
 * @date 2023-02-03
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MessageListener {

    String value() default "";

    String topic() default "";

    String channel() default "";

    String streamKey() default "";

    String consumerGroup() default "";

    String consumerName() default "";

    boolean pending() default false;

    Mode mode() default Mode.TOPIC;

    enum Mode {
        /**
         * topic 模式，主题订阅
         */
        TOPIC(),
        /**
         * pub/sub 模式 订阅发布 广播
         */
        PUBSUB(),
        /**
         * stream 模式
         */
        STREAM()
    }
}
